import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity,} from 'react-native';
import React, { useState, useEffect } from 'react';

import * as Location from 'expo-location';

export default function App() {

  const jours = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

  const Item = ({item, date}) => {
    return (
      <View style = {styles.jourMeteo}>
        <Text>{item.temperature}</Text>
        <Image source={{ uri: "https://openweathermap.org/img/wn/"+item.icone }} />
        <Text>{item.description}</Text>
        <Text style= {styles.gras}>{jours[date.getDay()]}</Text>
      </View>
    );
  };  

  const renderItem = ({item}) => {
    return (
      <Item
        item={item}
        date= {new Date(item.date)}
      />
    );
  };

  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);

  const APIkey = "a357b195c8d84492465ab145f80cc18d"

  const [latitude, setLatitude] = useState("");
  const [longitude, setLongitude] = useState("");
  const [ville, setVille] = useState("");
  const [temp, setTemp] = useState("");
  const [icone, setIcone] = useState("");
  const [nom, setNom] = useState("");
  const [prevision, setPrevision] = useState([]);

  useEffect(() => {
    (async () => {
      
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
      setLatitude(location.coords.latitude);
      setLongitude(location.coords.longitude);
  
    })();
  }, []);


  useEffect(() => {
    fetch(`https://api.openweathermap.org/geo/1.0/reverse?lat=${latitude}&lon=${longitude}&appid=${APIkey}`)
      .then(response => response.json())
      .then(json => {
        setVille(json[0].name);
      }).catch(error => {
        console.error(error);
      });

      fetch(
        `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${APIkey}&units=metric&lang=fr`)
      .then(response => response.json())
      .then(json => {
        setTemp(json.main.temp+"°C");
        setIcone(json.weather[0].icon+".png");
        setNom(json.weather[0].description);
      }).catch(error => {
        console.error("error :",error);
      });

      fetch(`https://api.openweathermap.org/data/2.5/forecast?lat=${latitude}&lon=${longitude}&appid=${APIkey}&units=metric&lang=fr`)
      .then(response => response.json())
      .then(json => {
        let meteo = [];
        for (let i in json.list){
          let date = new Date(json.list[i].dt_txt);
          if (date.getHours()==15){
            meteo.push({date: json.list[i].dt_txt, temperature: json.list[i].main.temp, 
              icone: json.list[i].weather[0].icon+".png",
              description: json.list[i].weather[0].description
            })
          }
        }
        setPrevision(meteo);
      }).catch(error => {
        console.error(error);
      });


  }, []);

  let text = 'Waiting..';
  if (errorMsg) {
    text = errorMsg;
  } else if (location) {
    text = "Vous êtes à "+ville;
    
  }

  return (
    <View style={styles.container}>
      <Text>{text}</Text>
      <Text style={styles.titre}>La méteo d'aujoud'hui</Text>
      <View style={styles.currentMeteo}>
        <Text>{temp}</Text>
        <Image source={{uri: "https://openweathermap.org/img/wn/"+icone}} style={{width: 40, height: 40}} />
        <Text>{nom}</Text>
      </View>
      <View style={styles.futureMeteo}>
        <Text style={styles.gras}>Météo des prochains jours à 15h:</Text>
      {prevision && 
        (
            <FlatList
              horizontal='true'
              data={prevision}
              renderItem={renderItem}
            />
        )}
      </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  currentMeteo: {
    width: 100,
    alignItems: 'center',
    borderColor: "red",
    borderWidth: 3,
    marginTop: 30,
    marginBottom: 30,
  },
  futureMeteo: {
    height: 200,
  },
  jourMeteo: {
    width: 100,
    alignItems: 'center',
    borderColor: "black",
    borderWidth: 3,
    marginLeft: 70,
    marginTop: 5,
    marginBottom: 5,
  },
  titre: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 5,
    marginBottom: 5,
  },
  gras: {
    fontWeight: "bold",
  }
});
